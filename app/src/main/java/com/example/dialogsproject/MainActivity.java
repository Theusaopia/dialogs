package com.example.dialogsproject;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void obterData(View view) {
        DatePickerDialog datePicker = new DatePickerDialog(this);
        datePicker.setOnDateSetListener(this);
        datePicker.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        TextView tv = (TextView) findViewById(R.id.txtData);
        tv.setText("Nascimento "+i2+"/"+i1+"/"+i);
    }

    public void sair(View view) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle("Confirmação");
        //bld.setMessage("Deseja sair mesmo sim ou não");
        final View v = getLayoutInflater().inflate(R.layout.dialog_dados, null);
        bld.setView(v);
        bld.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String edNome = ((EditText) v.findViewById(R.id.edNome))
                        .getText()
                        .toString();
                String edEmail = ((EditText) v.findViewById(R.id.edEmail))
                        .getText()
                        .toString();

                Toast.makeText(MainActivity.this, "Nome:" +edNome+ " Email: "+edEmail, Toast.LENGTH_LONG).show();
                finish();
            }
        });
        bld.setCancelable(false);
        bld.setNegativeButton("Não", null);
        bld.show();
    }
}